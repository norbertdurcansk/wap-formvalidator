//Norbert Durcansky 2MIT
//xdurca01 - WAP
//FIT VUTBR
//2017/2018

(function () {
  var FORM_SELECTOR = "form-validator";
  var MAX_LENGTH_SELECTOR = "data-validator-maxlength";
  var MIN_LENGTH_SELECTOR = "data-validator-minlength";
  var REQUIRED_SELECTOR = "data-validator-required";
  var FORMAT_SELECTOR = "data-validator-format";
  var MAXLENGTH_PROPERTY = "maxLength";
  var MINLENGTH_PROPERTY = "minLength";
  var REQUIRED_PROPERTY = "required";
  var FORMAT_PROPERTY = "format";
  var INPUT_STATE = {
    EMPTY: "EMPTY",
    SUCCESS: "SUCCESS",
    ERROR: "ERROR",
  };
  var elementsTree = [];
  // Ecm6 function converted for Browser support {obj1,...obj2}
  var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };

  // Ecm6 function converted for Browser support {[prop]:value}
  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key,
          {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
          });
    } else {
      obj[key] = value;
    }
    return obj;
  }

  /**
   *  Function retrieves value from data attribute
   * @param element  - Input element
   * @param dataSelector - Data attribute selector
   * @param property - Property in tree object
   */
  var getValueBySelector = function (element, dataSelector, property) {
    if (element.hasAttribute(dataSelector)) {
      var value = element.getAttribute(dataSelector);
      if (dataSelector === REQUIRED_SELECTOR) {
        return _defineProperty({}, property, value === "true")
      } else if (dataSelector === FORMAT_SELECTOR) {
        return _defineProperty({}, property, value)
      } else if (!isNaN(Number(value))) {
        return _defineProperty({}, property, value)
      }
    }
  };

  /**
   * Function retrieves submit input elements for specific form
   * @param form - DOM element
   * @returns {*|Array.<T>} - Array of submit elements
   */
  var getSubmitElementsForForm = function (form) {
    return [].slice.call(form.querySelectorAll('input[type="submit"]'));
  };

  /**
   * Function checks if the specific form is valid
   * @param index - Index in tree structure of forms
   * @returns {*} -  Boolean
   */
  var isFormValid = function (index) {
    if (elementsTree[index]) {
      return elementsTree[index].isValid()
    }
    return false;
  };
  /**
   * Function styles form and updates it's state
   * @param type - Style type
   * @param input - DOM element
   * @param submit - DOM element
   * @param formIndex - index in tree structure
   */
  var updateForm = function (type, input, submit, formIndex) {
    switch (type) {
      case INPUT_STATE.EMPTY:
        input.classList.remove("has-error");
        input.classList.remove("has-success");
        break;
      case INPUT_STATE.SUCCESS:
        input.classList.remove("has-error");
        input.classList.add("has-success");
        break;
      case INPUT_STATE.ERROR:
        input.classList.add("has-error");
        input.classList.remove("has-success");
        submit.forEach(function (btn) {
          btn.setAttribute("disabled", "true")
          btn.classList.add("has-error");
        });
        return;
    }
    if (isFormValid(formIndex)) {
      submit.forEach(function (btn) {
        btn.removeAttribute("disabled");
        btn.classList.remove("has-error");
      });
    }
  };

  /**
   * Function sets input listeners on change event and returns input's tree structure
   * @param form - DOM element
   * @param submit - Submit elements
   * @param formIndex - Index in tree structure
   * @returns {*} - Array of input tree structure with listeners
   */
  var getInputElements = function (form, submit, formIndex) {
    return [].slice.call(form.querySelectorAll('input[type="text"]'))
        .reduce(function (acc, input) {
              var conditions = {};
              // get condition object for each data selector
              _extends(conditions, {required: false}); // default value
              _extends(conditions, getValueBySelector(input,
                  MAX_LENGTH_SELECTOR, MAXLENGTH_PROPERTY));
              _extends(conditions, getValueBySelector(input,
                  MIN_LENGTH_SELECTOR, MINLENGTH_PROPERTY));
              _extends(conditions, getValueBySelector(input,
                  REQUIRED_SELECTOR, REQUIRED_PROPERTY));
              _extends(conditions, getValueBySelector(input,
                  FORMAT_SELECTOR, FORMAT_PROPERTY));
              // add isValid function
              _extends(conditions, {
                isValid: function () {
                  return isValid(conditions, input)
                }
              });
              _extends(conditions, {
                element: input
              });
              if (Object.keys(conditions).length !== 0) {
                var inputChange = function () {
                  if (isValid(conditions, input)) {
                    updateForm((!conditions.required && input.value.length === 0) ?
                        INPUT_STATE.EMPTY : INPUT_STATE.SUCCESS, input, submit, formIndex);
                  } else {
                    updateForm(INPUT_STATE.ERROR, input, submit, formIndex);
                  }
                };
                input.addEventListener("input", inputChange);
                //initial validation call
                inputChange();
              }
              acc.push(conditions);
              return acc;
            }
            ,
            []
        )
  };

  /**
   * Function validates condition object with input
   * @param conditions  - Object with condition for specific input
   * @param input - DOM element
   * @returns {boolean} - returns true/false
   */
  var isValid = function (conditions, input) {
    var valid = true;
    var valueLength = input.value.length;
    if (valueLength === 0 && !conditions.required) {
      return true;
    } else {
      valid = valid && (conditions.required ? valueLength !== 0 : true);
    }
    if (MAXLENGTH_PROPERTY in conditions) {
      valid = valid && (conditions.maxLength >= valueLength)
    }
    if (MINLENGTH_PROPERTY in conditions) {
      valid = valid && (conditions.minLength <= valueLength)
    }
    if (FORMAT_PROPERTY in conditions) {
      try {
        var regex = new RegExp(conditions.format);
        valid = valid && regex.test(input.value)
      } catch (e) {
        console.error(e, "Provided bad regex.")
        valid = false;
      }
    }
    return valid;
  };

  /**
   * Main Function, on DOM loaded event creates tree structure of form elements
   */
  document.addEventListener("DOMContentLoaded", function () {
    // get Tree structure of form elements
    elementsTree = [].slice.call(
        document.querySelectorAll('form[data-toggle="' + FORM_SELECTOR + '"]'))
        .reduce(function (acc, form, index) {
          var submitElements = getSubmitElementsForForm(form);
          var inputs = getInputElements(form, submitElements, index);
          acc.push({
            form: form,
            submitElements: submitElements,
            inputs: inputs,
            index: index,
            isValid: function () {
              return inputs.filter(function (input) {
                return !input.isValid()
              }).length === 0
            }
          });
          return acc;
        }, []);
  });
}());